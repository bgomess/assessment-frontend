/*
|
|   Scripts para utilização no teste de avaliação de conhecimentos da Webjump
|   
|   Biblioteca utilizada: JQuery
|
*/
$(document).ready(function () {
    var categories = {
        "categorias": [
            {
                "id": 1,
                "name": "Roupas",
                "path": "roupas"
            },
            {
                "id": 2,
                "name": "Sapatos",
                "path": "sapatos"
            },
            {
                "id": 3,
                "name": "Acessórios",
                "path": "acessorios"
            }
        ]};


    var types = {
        "tipos": [
            {
                "id": 1,
                "name": "Corrida",
                "path": "corrida"
            },
            {
                "id": 2,
                "name": "Caminhada",
                "path": "caminhada"
            },
            {
                "id": 3,
                "name": "Casual",
                "path": "casual"
            },
            {
                "id": 4,
                "name": "Social",
                "path": "social"
            }
        ]};

    var colors = {
        "cores": [
            {
                "id": 1,
                "name": "Vermelho",
                "path": "red"
            },
            {
                "id": 2,
                "name": "Laranja",
                "path": "orange"
            },
            {
                "id": 3,
                "name": "Azul",
                "path": "blue"
            }
        ]};

    // Monta o menu com os dados obtidos por api
    makeMenu();

    // Popula as categorias
    $.each(categories.categorias, function(index, value) {
        $('#categories').append("<li class='text-category' id='cat_"+value.path+"'>"+value.name+"</li>");
    });

    // Popula os tipos
    $.each(types.tipos, function(index, value) {
        $('#types').append("<li class='text-category' id='type_"+value.path+"'>"+value.name+"</li>");
    });

    // Monta os painéis com as informações dos produtos com base nos dados obtidos por api
    getProducts(1,0,false,false); // Camisetas
    getProducts(2,0,false,false); // Calças
    getProducts(3,0,false,false); // Tênis
    //getProducts(4,0,false); // Acessórios (não cadastrado)

    // Define os filtros com base nas categorias
    $.each(categories.categorias, function(index, value) {
        $('#cat_'+value.path).on('click',function() {
            $('#products').empty();
            $('#title_products, .item-breadcrumb').html(value.name);

            if(index+1 == 1) { // roupas
                if(!getProducts(1,0,false,false) || !getProducts(2,0,false,false))
                    $('#products').html('Não há produtos disponíveis nessa categoria.');
            }

            if(index+1 == 2) { // sapatos
                if(!getProducts(3,0,false,false))
                    $('#products').html('Não há produtos disponíveis nessa categoria.');
            }
        });
    });

    // Define os filtros com base nas cores
    $.each(colors.cores, function(index, value) {
        $('.'+value.path).on('click',function() {
            $('#products').empty();

            getProducts(1,0,value.name,false);
            getProducts(2,0,value.name,false);
            getProducts(3,0,value.name,false);
        });
    });

    // Define os filtros com base nos tipos
    $.each(types.tipos, function(index, value) {
        $('#type_'+value.path).on('click',function() {
            $('#products').empty();
            $('#title_products, .item-breadcrumb').html(value.name);

            getProducts(1,value.path,false,false);
            getProducts(2,value.path,false,false);
            getProducts(3,value.path,false,false);
        });
    });

    // Clique do menu "camisetas"
    $('#menuitens').on('click','.menuitem-1',function() {
        $('#products').empty();
        $('#title_products, .item-breadcrumb').html('Camisetas');

        if(!getProducts(1,0,false,false))
            $('#products').html('Não há produtos disponíveis nessa categoria.');
    });


    // Clique do menu "calças"
    $('#menuitens').on('click','.menuitem-2',function() {
        $('#products').empty();
        $('#title_products, .item-breadcrumb').html('Calças');

        if(!getProducts(2,0,false,false))
            $('#products').html('Não há produtos disponíveis nessa categoria.');
    });

    // Clique do menu "sapatos"
    $('#menuitens').on('click','.menuitem-3',function() {
        $('#products').empty();
        $('#title_products, .item-breadcrumb').html('Sapatos');

        if(!getProducts(3,0,false,false))
            $('#products').html('Não há produtos disponíveis nessa categoria.');
    });

    // Clique do menu "sapatos"
    $('#menuitens').on('click','.menuitem-contato',function() {
        alert('Bruno Gomes da Silva - (61) 99129 2938 - bruno.gdsilva@gmail.com');
    });

    // Pesquisa
    $('.btn-search').on('click', function() {
        $('#products').html("");
        getProducts(1,0,false,$('.input-search').val());
        getProducts(2,0,false,$('.input-search').val());
        getProducts(3,0,false,$('.input-search').val());
    });
});

/*--------------------------------------------------------------------------------------------------
    Configura o menu com base nas informações obtidas por api
    Sem retorno
*/
function makeMenu() {
    // Recupera os dados via API
    $.get("mock-api/V1/categories/list", function(data) {
        let obj = JSON.parse(data);

        // Iteração dos dados
        $.each(obj.items, function(index, value) {
            // Popula o menu
            $('#menuitens').append(
                '<li id="itemmenu_'+value.id+'" class="menuitem-'+value.id+'">\
                    <a >' + value.name.toUpperCase() + '</a>\
                </li>');
        });

        // Inclui o último item de menu após a recuperação dos dados
        $('#menuitens').append(
            '<li class="menuitem-contato">\
                <a href="#">CONTATO</a>\
            </li>');
    });
}

/*--------------------------------------------------------------------------------------------------
    Recupera os produtos com base nas informações obtidas por api
    Parâmetros:
        - category: categoria do produto (1. Camisetas; 2. Calças; 3. Tênis)
        - type: tipo de produto (0. Todos os tipos; 1. Corrida; 2. Caminhada; 3. Casual; 4. Social)
        - color: string do nome da cor no caso de filtro por cor
    Retorno:
        - true: sucesso na operação
        - false: erro de acesso à api (404)
*/
function getProducts(category,type,color,search) {
    var flag = true; // Indicador de erro

    // Recupera os dados via API
    $.get("mock-api/V1/categories/"+category)
        // Sucesso
        .done(function(data) {
            let obj = JSON.parse(data);

            // Iteração dos dados
            $.each(obj.items, function(index, value) {
                if(color) { // filtro por cor
                    if(value.filter[0].color == color) {
                        makeProductPanel(value);
                    }
                } else if(type != 0) { // filtro por tipo
                    console.log(type);
                    if(value.type == type)
                        makeProductPanel(value);
                } else if(search) { // busca por palavra
                    if(value.name.toUpperCase().indexOf(search.toUpperCase()) != -1)
                        makeProductPanel(value);
                } else { // sem filtro
                    makeProductPanel(value);
                }
            });
        })
        // Falha
        .fail(function(){
            flag = false;
        });

    return flag;
}

/*--------------------------------------------------------------------------------------------------
    Configura a div de cada produto
    Parâmetros: valor do produto atual na iteração
*/
function makeProductPanel(value) {
    $('#products').append(
        '<div class="panel-product">\
            <img src="'+value.image+'">\
            <br>\
            '+value.name+'\
            <br>\
            <label class="product-price">R$'+value.price+'</label>\
            <br>\
            <button>Comprar</button>\
        </div>');
}